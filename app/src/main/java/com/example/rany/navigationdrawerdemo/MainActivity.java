package com.example.rany.navigationdrawerdemo;

import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.example.rany.navigationdrawerdemo.fragments.HistoryFragment;
import com.example.rany.navigationdrawerdemo.fragments.HomeFragment;
import com.example.rany.navigationdrawerdemo.fragments.LogoutFragment;


public class MainActivity extends AppCompatActivity {

    NavigationView navigationView;
    DrawerLayout drawerLayout;
    Toolbar toolbar;
    ActionBarDrawerToggle toggle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        navigationView = findViewById(R.id.navigation_view);
        drawerLayout = findViewById(R.id.drawer_view);

        // set toolbar
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // set icon to action bar
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(R.drawable.ic_format_list_bulleted_black_24dp);

        toggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.slideup,
                R.string.slidedown){
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }
        };

        // event on navigation view
        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                Fragment fragment = null;
                Class classFragment = null;
                switch (item.getItemId()){
                    case R.id.itmHome :
                        classFragment = HomeFragment.class;
                        break;
                    case R.id.itmFavorite :
                        classFragment = HistoryFragment.class;
                        break;
                    case R.id.itmHistory :
                        classFragment = HistoryFragment.class;
                        break;
                    case R.id.itmProfile :
                        classFragment = HistoryFragment.class;
                        break;
                    case R.id.logout :
                        classFragment = LogoutFragment.class;
                        break;
                }
                // create object and set to each fragment
                try {
                    fragment = (Fragment) classFragment.newInstance();
                } catch (InstantiationException e) {
                    e.printStackTrace();
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
                // add run time fragment
                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.container_frame, fragment);
                transaction.commit();
                // set title to action bar
                setTitle(item.getTitle());
                // close drawer after item has been select
                drawerLayout.closeDrawers();

                return true;
            }
        });
    }
}
